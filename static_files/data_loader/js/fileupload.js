var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
    var ws_path = ws_scheme + '://' + window.location.host + "/api/chat/stream/";
    var newWindow = null;
    console.log(ws_scheme);
    console.log(ws_path );
    var ws = new ReconnectingWebSocket(ws_path);

    $(document).on("change","select",function(){
      $("option[value=" + this.value + "]", this)
      .attr("selected", true).siblings()
      .removeAttr("selected")
    });

    function sendRequest() {
        var blob = document.getElementById('fileToUpload').files[0];
        const BYTES_PER_CHUNK = 104857600; // 1MB chunk sizes.
        const SIZE = blob.size;
        var start = 0;
        var end = BYTES_PER_CHUNK;

        while( start < SIZE ) {
            var chunk = blob.slice(start, end);
            uploadFile(chunk);
            start = end;
            end = start + BYTES_PER_CHUNK;
        }
        openNewWindow();
    }

    function openNewWindow(){
        console.log("trying to open a new window...")
        newWindow = window.open("", "MsgWindow", "width=400,height=50, top=750, left=1200")

        var elem = document.createElement('div');
        elem.setAttribute("id", "myBar");
        elem.style.cssText = 'position:absolute;width:95%;height:50%;opacity:0.3;z-index:100;background:#4CAF50';
        newWindow.document.body.appendChild(elem);
    }

    function move(width) {
          var elem = newWindow.document.getElementById("myBar");
          var id = setInterval(frame, 10);
          function frame() {
            if (width >= 100) {
                clearInterval(id);
            } else {
              width++;
              elem.style.width = width + '%';
            }
          }
          var y = newWindow.document.createTextNode(Math.round(width) + '%');
          elem.appendChild(y);
    }

    function fileSelected() {
        var file = document.getElementById('fileToUpload').files[0];
        if (file) {
            var fileSize = 0;
            if (file.size > 1024 * 1024)
                fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
            else
                fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

            document.getElementById('fileName').innerHTML = 'Name: ' + file.name;
            document.getElementById('fileSize').innerHTML = 'Size: ' + fileSize;
            document.getElementById('fileType').innerHTML = 'Type: ' + file.type;

            var dropDown = document.getElementById("ModuleDropDown");
            var dpVal = dropDown.options[dropDown.selectedIndex].value;

            init_params = {};
            init_params.action = 'prepare';
            init_params.file_name = file.name;
            init_params.file_size = fileSize;
            init_params.moduleName = dpVal;

            ws.send(JSON.stringify(init_params))
            console.log("sending init params.....")

        }
    }
    function executeAsync(func) {
        setTimeout(func, 0);
    }

    function uploadFile(blobFile) {
        executeAsync(function() {
        ws.send(blobFile);
        console.log("after sending via websocket...")

        });
    }

    function uploadProgress(evt) {
        if (evt.lengthComputable) {
            var percentComplete = Math.round(evt.loaded * 100 / evt.total);
            document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
        }
        else {
            document.getElementById('progressNumber').innerHTML = 'unable to compute';
        }
    }

    function uploadComplete(evt) {
        /* This event is raised when the server send back a response */
        alert(evt.target.responseText);
    }

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.");
    }

    function uploadCanceled(evt) {
        ws.close();
        console.log("closed the chat socket...");
        //alert("The upload has been canceled by the user or the browser dropped the connection.");
    }
    ws.onmessage = function (message) {

        newWindow.console.log("Got websocket message " + message.data);
        // Decode the JSON
        var data = JSON.parse(message.data);

        // Handle errors
        if (data.error) {
            alert(data.error);
            return;
        }
        percentage = data.percent;
        newWindow.console.log("percentage is:: " + percentage);
        move(percentage);
        if(data.action == "complete"){
            newWindow.console.log("uploaded successfully...")
        }
    };