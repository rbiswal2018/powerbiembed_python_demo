from django.contrib.auth.models import User, Group
from django.db import models

# Create your models here.


class RoleManagementNew(models.Model):
    """
    to store role details
    """
    role_name = models.CharField(null=False, max_length=128)
    group = models.ForeignKey(Group, on_delete=models.PROTECT)
    fname = models.CharField(max_length=128, null=False, blank=False, default='dummyfname')
    lname = models.CharField(max_length=128, null=True, blank=True)
    emailid = models.CharField(max_length=128, null=False, blank=False, default='dummy@gmail.com')
    is_admin = models.BooleanField(default=False)

    class Meta:
        managed = True
        ordering = ['pk']
        db_table = "role_management"


class UserProfileNew(models.Model):
    """
    to save user's data
    """
    email = models.CharField(max_length=128, blank=False, null=False)
    mobile = models.CharField(max_length=15, blank=True, null=True)
    token_key = models.CharField(max_length=128, null=True, blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.ForeignKey(RoleManagementNew, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.email

    class Meta:
        managed = True
        db_table = 'user_profile'









