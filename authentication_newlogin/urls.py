from django.conf.urls import url
from django.urls import path

from authentication_newlogin import views

urlpatterns = [
    # path('', views.index, name='login'),
    path('login/', views.login, name='login'),
    path('registration/', views.create_user, name='login'),
    path('forgot_password/', views.forgot_password, name='forgot_pass'),
    path(r'reset_password/<str:token_key>/', views.reset_pass, name='reset_pass'),
    # url(r'reset_password/(.*)/$', views.reset_pass, name='reset_pass'),
    # path(r'^reset_password/anythingfornow/', views.reset_pass, name='reset_pass'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('logout/', views.logout_user, name='logout'),
    path('user_management/', views.user_management, name='user_management'),
    path('user_management/getusers/', views.get_users, name='getusers'),
    path('user_management/adduser', views.add_user, name='adduser'),


]
