from django.contrib.auth import authenticate, logout, login as django_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.core.mail import EmailMessage
import json
from django.http import HttpResponse
from django.shortcuts import HttpResponseRedirect
from django.template import RequestContext
from django.template.context_processors import csrf
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect, csrf_exempt
import _thread as thread
import secrets

from authentication_newlogin.models import UserProfileNew, RoleManagementNew


@csrf_exempt
def index(request):
    """
    put home page here...as of now login page
    :param request:
    :return:
    """
    return HttpResponseRedirect("/login")


@csrf_exempt
def login(request):
    """

    :param request:
    login the user here
    :return:
    """

    csrf(request)
    error = ''
    if request.method == 'POST':
        email = request.POST.get("email")
        password = request.POST.get("pass")

        if User.objects.filter(username=email).exists():
            user = authenticate(username=email, password=password)
            if user:
                django_login(request, user)
                return HttpResponseRedirect('/api/auth/newlogin/dashboard')
            else:
                error = 'either password or email id is wrong'
        else:
            error = 'email id not registered with us.'

    return render(request, "login.html", {'error': error})


@csrf_protect
def create_user(request):
    """
    create user's account here based on registration form
    :param request:
    :return:
    """
    print("in create_user function as user is new")
    error = ''
    if request.method == "POST":
        email = request.POST.get("email")
        confirm_pass = request.POST.get("confirmpass")
        mobile = request.POST.get("mobile")

        user_obj = User(username=email)
        user_obj.set_password(confirm_pass)
        user_obj.save()

        user_profile = UserProfileNew()
        user_profile.email = email
        user_profile.mobile = mobile
        user_profile.user_id = user_obj.id
        user_profile.save()
        error = "user created successfully"
    else:
        error = "user could not be created"
    return render(request, "registration.html", {"error": error})


@csrf_protect
def forgot_password(request):
    """
    send mail to user's account if user forgets his/her password
    :param request:
    :return:
    """

    reset_status = ''
    error = ''
    if request.method == "GET":
        return render(request, "forgot_password.html")
    elif request.method == "POST":
        forgot_email = request.POST.get("forgot_email")
        try:
            user_profile_obj = UserProfileNew.objects.get(email=forgot_email)
            if user_profile_obj:
                # token_key = "anythingfornow"
                token_key = secrets.token_urlsafe(20)
                print("token_key is::: %s" % token_key)
                user_profile_obj.token_key = token_key
                user_profile_obj.save()
                from_email, to_email, subject = "rajakumar.biswal@gmail.com", forgot_email, "Reset Password"
                html_content = "<html><body><p>click on link or copy the link and paste in browser url</p>" \
                               + "<br><br>http://127.0.0.1:8000/reset_password/" \
                               + token_key + "/</body></html>"
                thread.start_new_thread(send_email, (subject, html_content, forgot_email, to_email))
                reset_status = "mail is sent, check your mail %s" % forgot_email
        except UserProfileNew.DoesNotExist:
            reset_status = "mail id %s is not registered with us" % forgot_email

    return_dict = {
        "reset_status": reset_status,
        "emailId": forgot_email,
        "error": error
    }

    return render(request, "forgot_password.html", return_dict)


def send_email(subject, html_content, from_email, to):
    """
        Method to send email using thread
    """
    print("From send email method")
    try:
        msg = EmailMessage(subject, html_content, from_email, [to])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()
    except Exception as e1:
        print("coming here as exception occurred")


@csrf_exempt
def reset_pass(request, token_key):
    """
    update user's account password in case user forgets
    :param request:
    :return:
    """
    print("inside reset_pass function")
    error = ''
    user_email = ''
    if request.method == "GET":
        return render(request, "reset_password.html")
    elif request.method == "POST":
        cnfrmpass = request.POST.get("cnfrmpass")
        user_email = request.POST.get("user_email")
        try:
            user = User.objects.get(username=user_email)
            print("user value is:: %s" % user)
            istokenpresent = UserProfileNew.objects.get(token_key=token_key)
            print("Token Key: %s" % token_key)
            print("istokenpresent Key: %s" % istokenpresent)
            if user is not None and istokenpresent is not None:
                print("updating user password now...")
                user.set_password(cnfrmpass)
                user.save()
                error = "password updated successfully"
                return HttpResponseRedirect('/login')
        except User.DoesNotExist:
            print("email id is not registered with us.Please check.")
            error = "email id is not registered with us.Please check."
        except UserProfileNew.DoesNotExist:
            print("token key is not registered with us")
            error = "token key is not registered with us."

    return_dict = {
        "error": error,
        "emailId": user_email
    }

    return render(request, "reset_password.html", return_dict)


@login_required
def dashboard(request):
    # print("in dashboard function")
    return render(request, "dashboard.html")


def logout_user(request):
    """
        This function is used to logout the user .
    :param request:
    :return:
    """
    # print("Session Expired as logout is called")
    logout(request)
    request.session.flush()
    return HttpResponseRedirect('/api/auth/newlogin/login/')


@login_required
def user_management(request):
    # print("in user management function...")
    return render(request, "user_management.html")


@login_required
def get_users(request):
    # print("inside get_users function")
    if request.method == "GET":
        users = RoleManagementNew.objects.all()
        users_list = list()
        columns = ["role_name", "group_id", "fname",  "lname", "emailid", "is_admin"]
        columns = [{"title": name} for name in columns]
        for u in users:
            templist = list()
            templist.append(u.role_name)
            templist.append(u.group_id)
            templist.append(u.fname)
            templist.append(u.lname)
            templist.append(u.emailid)
            templist.append(u.is_admin)
            users_list.append(templist)
        return HttpResponse(json.dumps({"data": users_list, "columns": columns}, ensure_ascii=False))


@login_required
def add_user(request):
    # print("inside add user function")
    error = ''
    if request.method == "POST":
        try:
            is_adminuser = RoleManagementNew.objects.get(emailid=request.user).role_name
            # print(is_adminuser)
            if is_adminuser == "admin":
                # print("control is here111")
                firstname = request.POST.get("firstname")
                lastname = request.POST.get("lastname")
                emailid = request.POST.get("emailid")
                role = request.POST.get("role")
                group_obj = Group.objects.get(name=role)
                role_manage_obj = RoleManagementNew(role_name=role, group_id=group_obj.id, fname=firstname)
                role_manage_obj.role_name = role
                role_manage_obj.emailid = emailid
                if role == "admin":
                    # print("control is here222")
                    role_manage_obj.is_admin = True
                role_manage_obj.lname = lastname
                role_manage_obj.save()
                error = "user role created successfully."
                # print("user role is created successfully.")
        except Exception as e2:
            error = "Only admin users can add an user"
            # print("Only admin users can add an user,role couldn't be created")
    return render(request, "user_management.html", {error: error})



